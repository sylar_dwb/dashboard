// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './styles/main.scss'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import './../static/font/ledfont/font.css'
import './../static/font/ilineFont/iconfont.css'
import moment from 'moment'
import $ from 'jquery'
Vue.prototype.$moment = moment
Vue.prototype.$ = $
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
