import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import main from '@/views/main'
import home from '@/views/home'
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/main/:type',
    name: 'main',
    component: main
  },
  {
    path: '*',
    component: home
  }
  ]
})
