const main = {
  state: {
    refresh: true
  },
  mutations: {
    UPDATE_REFRESH_STATE(state) {
      state.refresh = !state.refresh
    }
  },
  actions: {

  }
}
export default main
