# test

> A Vue.js project

## Build Setup

``` bash


# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build


# npm install 之后，chromedriver安装失败,请单独安装：
npm install chromedriver --chromedriver_cdnurl=http://cdn.npm.taobao.org/dist/chromedriver

# npm install ，启动时报错，‘node_modules/node-sass/vendor’,解决方案：
npm rebuild node-sass

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

